#include "fisher2020_motion.h"

#include "fisher2020_sensors.h"

#include "ev3.h"
#include "ev3_tacho.h"

#define SPEED_FACTOR speed_factor	//0.20
#define TOO_CLOSE 2700

/* The robot should go forward until detecting an object at a distance superior or equal to <distance_to> in front of him.
 * /!\ Will require the sensor interface.
 */
int forward_to(int distance_to, float speed_factor){

	int distance = get_distance_us();
	int sign = 1;
	if(distance-distance_to<0){
		sign = -1;
	}

	if(SPEED_FACTOR<0 || SPEED_FACTOR>1){
		return -1;
	}

	set_tacho_speed_sp(m_left_tacho, (int)(sign*SPEED_FACTOR*max_speed_tacho));
	set_tacho_speed_sp(m_right_tacho, (int)(sign*SPEED_FACTOR*max_speed_tacho));
	set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
	set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
	
	distance = distance_to+sign*1;
	do{
		distance = (int) get_distance_us();
	}while(sign*distance > sign*distance_to && distance<TOO_CLOSE);
	
	set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);
	
	return 1;

}

