#include "fisher2020_sensors.h"

#include <stdlib.h>
#include <pthread.h>

/* It is needed to uninitialized the sensors process tracking (join the thread).
 */
int uninitialize_sensors(){

	if(sensors_thread_active != 1){
		return -1;
	}
	
	pthread_cancel(sensors_thread);
	
	pthread_mutex_unlock(&sensors_thread_mutex);
	
	pthread_mutex_destroy(&sensors_thread_mutex);
	
	sensors_thread_active = 0;
	
	return 0;

}

