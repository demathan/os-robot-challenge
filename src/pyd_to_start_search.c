#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"

#include "fisher2020.h"

#define BACK_DISTANCE 250

#define WALL_DISTANCE1 300

#define WALL_DISTANCE2 50



/* This function drives the robot from the pyramid to the search start position.
 */
int pyramid_to_start_search_position(){
	
	//First go backward.
	if(forward_of(-BACK_DISTANCE,0.20f)<0){
		return -1;
	}
	sleep(3);
	
	//Then turn left.
	if(turn(-90)<0){
		return -1;
	}
	
	return 0;
}

#ifdef TEST_PYRAMID_TO_START_POS

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	return pyd_to_start_pos();
}

#endif


