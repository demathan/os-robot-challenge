#ifndef FISHER2020_SENSORS

#include <stdlib.h>
#include <pthread.h>

#define FISHER2020_SENSORS

//Variables definition


#ifdef THREAD_MODE

//thread stuff

//thread
pthread_t sensors_thread;
int sensors_thread_active = 0;

//shared variables mutex
pthread_mutex_t distance_us_mutex;
pthread_mutex_t angle_gyro_mutex;
pthread_mutex_t angle_compass_mutex;
pthread_mutex_t measure_color_mutex;

//shared variables
int distance_us;
int angle_gyro;
int angle_compass;
int measure_color;

#else

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"

uint8_t sn_us;
uint8_t sn_gyro;
uint8_t sn_compass;
uint8_t sn_color;

#endif

//TODO Is there anything to add here? Maybe shared variables.

/* Initialize sensors value tracking and all the needed objects.
 */
int initialize_sensors();

#ifdef THREAD_MODE

/* It is needed to uninitialized the sensors process tracking (cancel the thread).
 */
int uninitialize_sensors();

#endif

/* Returns the estimated distance to an obstacle given by the ultrasonic sensor.
 */
float get_distance_us();

/* Returns the angle measured by the gyroscope.
 */
float get_angle_gyro();

/* Returns the angle measured by the compass.
 */
float get_angle_compass();

/* Returns a characteristic value of the color sensor.
 */
float get_measure_color();

//TODO Other functions to add?

#ifdef THREAD_MODE

/* The function run by the sensors thread
 */
void * sensors_thread_function(void * arg);

#endif

#endif

