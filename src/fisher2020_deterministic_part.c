#include "fisher2020.h"
#include "fisher2020_motion.h"

#define NB_BALLS 4

#ifdef TEST_FISHER2020_DETERMINISTIC_PART

int main(int argc, char * argv[]){
	
	if(initialization()<0){
		return -1;
	}
	return fisher2020_deterministic_part();
	
}

#endif

/* This function performs the first part of the challenge.
 * /!\ This fuction returns -1 in case of an error, 1 if it managed to put all the balls in the destination pyramid and 0 if it gave up at some point.
 */
int fisher2020_deterministic_part(){
	
	int status=0;
	
	//first go to the origin cube.
	if(start_pos_to_origin_cube()<0){
		return -1;
	}
	
	//There are 4 balls to get from the cube to the pyramid.
	for(int i=0; i<NB_BALLS; i++){
		
		//Try to grab a ball until grabing one.
		if(forward_of(50,0.05)<0){
			return -1;
		}
		sleep(1);
		status=0;
		if(crane_take()<0){
			return -1;
		}
		while(is_there_something_in_the_wrench()==0){
		
			//status represents different positions that the robot tries to grab a ball.
			status=status+1;
			if(status==1||status==6||status==4||status==9){
				if(turn(5)<0){
					return -1;
				}
			}
			if(status==2||status==7||status==3||status==8){
				if(turn(-5)<0){
					return -1;
				}
			}
			if(status==5){
				if(forward_of(30,0.05)<0){
					return -1;
				}
				sleep(1);
			}
			
			if(open_wrench()<0){
				return -1;
			}
			
			if(status==10){
				//give up
				break;
			}
			
			if(crane_take()<0){
				return -1;
			}
			
		}
		//Exiting the while loop means that a ball is in the wrench or that we gave up.
		
		if(status==10){
			//we gave up
			return 0;
		}
		
		//Position correction before going
		if(status==1||status==6||status==4||status==9){
			if(turn(-5)<0){
				return -1;
			}
		}
		if(status==2||status==7||status==3||status==8){
			if(turn(5)<0){
				return -1;
			}
		}
		
		//Go to the pyramid
		if(known_origin_cube_to_pyramid()<0){
			return -1;
		}
		
		//Drop the ball.
		if(crane_drop()<0){
			return -1;
		}
		
		//If it's the fouth ball, we can stop here.
		if(i==NB_BALLS-1){
			break;
		}
		
		//Else, go back to the cube.
		if(pyramid_to_origin_cube()<0){
			return -1;
		}
		
	}
	
	return 1;
	
}

