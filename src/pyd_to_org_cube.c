#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"
#include "fisher2020.h"

#define BACK_DISTANCE 250

#define WALL_DISTANCE1 300

#define WALL_DISTANCE2 200

#define CUBE_DISTANCE1 75

#define CUBE_DISTANCE2 35

#ifdef TEST_PYRAMID_TO_ORIGIN_CUBE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int angular_position_correction(float o_compass, int wished);

int pyramid_to_origin_cube();

float o_compass;

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	o_compass=get_angle_compass()+90.0f;
	printf("main: o_compass=%f\n",o_compass);
	return pyramid_to_origin_cube();
}

#endif

/* This function searches the randomly placed cube (starting from the start position of the robot).
 */
int pyramid_to_origin_cube(){
	
	//First, go a bit backward.
	if(forward_of(-BACK_DISTANCE,0.20f)<0){
		return -1;
	}
	sleep(3);
	
	//Then turn right.
	if(turn(90)<0){
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
		return -1;
	}
	
	//Go forward until being at 20cm from the wall in front.
	if(forward_to(WALL_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(WALL_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	//Turn right again.
	if(turn(90)<0){
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
		return -1;
	}
	
	//And forward until reaching the cube.
	if(forward_to(CUBE_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(CUBE_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	return 0;
}






