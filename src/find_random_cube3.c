#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"
#include "fisher2020.h"

#define INCREMENT 75

#define INITIAL 700

#define NB_SAMPLES 9

#ifdef TEST_RANDOM_CUBE_SEARCH_3

#include <stdio.h>

int main(int argc, char * argv[]){
	
	initialization();
	if(random_cube_search()<0){
		return(-1);
	}
	printf("random_origin_cube_side=%d\n",random_origin_cube_side);
	printf("random_origin_cube_distance=%d\n",random_origin_cube_distance);
	forward_to(1000,0.20f);
	forward_to(random_origin_cube_distance,0.05f);
	if(random_origin_cube_side==RANDOM_CUBE_ON_RIGHT_SIDE){
		turn(-90);
	}else{
		turn(90);
	}
	forward_to(35,0.05f);
	
}

#endif

/* This function searches the randomly placed cube (starting from the start position of the robot).
 * /!\ The robot starts from the middle of the arena (800 cm from the y- wall) and goes to it's starting position.
 */
int random_cube_search(){
	
	int target = INITIAL;
	
	int minimum = 3000;
	
	int value;
	
	for(int i=0; i<NB_SAMPLES; i++){
		
		//The robot faces the wall at y- and goes forward of an increment.
		if(forward_to(target,0.05f)<0){
			return -1;
		}
		
		//Look on the right side of the arena.
		if(turn(-90)<0){
			return -1;
		}
		value = (int) get_distance_us();
		if(value<minimum){
			minimum=value;
			random_origin_cube_distance=target;
			random_origin_cube_side=RANDOM_CUBE_ON_RIGHT_SIDE;
		}
		//Look on the left side of the arena.
		if(turn(90)<0){
			return -1;
		}
		if(turn(90)<0){	//yes, this is weird but I am trying to make the gyroscope more stable.
			return -1;
		}
		value = (int) get_distance_us();
		if(value<minimum){
			minimum=value;
			random_origin_cube_distance=target;
			random_origin_cube_side=RANDOM_CUBE_ON_LEFT_SIDE;
		}
		if(turn(-90)<0){
			return -1;
		}
		
		if(turn(0)<0){	//It's the better way to go straight.
			return -1;
		}
		
		target = target - INCREMENT;
		
	}
	
	return 0;
	
}

