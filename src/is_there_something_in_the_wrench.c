
#include "fisher2020.h"
#include "fisher2020_motion.h"

#include "fisher2020_sensors.h"

#include "ev3.h"
#include "ev3_tacho.h"
#include "ev3_servo.h"

#include <unistd.h>

#define THRESHOLD 10

int is_there_something_in_the_wrench(){

	sleep(1);

	int color = (int)get_measure_color();

	if(color > THRESHOLD){
		return 1;
	}
	return 0;
}
