#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"
#include "fisher2020.h"

#include <unistd.h>
#include <stdio.h>

#define FRONT_DISTANCE 500

#define CUBE_DISTANCE1 60

#define CUBE_DISTANCE2 20

#define WALL_DISTANCE1 45

#define WALL_DISTANCE2 20

#define N 10000
#define HALF_NB_SAMPLES 3

int obs_pos_y;
int obs_pos_left;
int obs_pos_right;
int valmin;
int argmin;
float relative_speed = 0.20;

#ifdef TEST_RANDOM_CUBE_SEARCH

int random_cube_search();

float o_compass;

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	o_compass=get_angle_compass()+90.0f;
	printf("main: o_compass=%f\n",o_compass);
	return random_cube_search();
}

#endif

/* This function searches the randomly placed cube (starting from the start position of the robot).
 */
int random_cube_search(){

	int vals[ 2 * HALF_NB_SAMPLES +1 ];
	
	
	do{	//The outer loop allows to repeat the research maneuver to get gradualy closer to a detected object.
	
		do{	//The inner loop allows to repeate a research maneuver until detecting something interesting.
	
			//old code kept just in case.
			/*set_tacho_time_sp(m_left_tacho, 1000);
			set_tacho_time_sp(m_right_tacho, 1000);
				set_tacho_speed_sp(m_left_tacho, (int)(0.15*speed));
			set_tacho_speed_sp(m_right_tacho, (int)(-0.15*speed));
			set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
			set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
			sleep(1);
			
			set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
			set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
			set_tacho_command_inx(m_left_tacho, TACHO_STOP);
			set_tacho_command_inx(m_rightc_tacho, TACHO_STOP);*/
			
			//The robot performs a fan-shaped maneuver.
			if(turn(90)<0){
				return -1;
			}
			
			obs_pos_y = (int)get_distance_us();

			vals[0]=obs_pos_y;
			
			for(int k =0; k< 2 * HALF_NB_SAMPLES ; k++){
			
				//old code kept just in case.
				/*set_tacho_time_sp(m_left_tacho, (int)(1200/M));
				set_tacho_time_sp(m_right_tacho, (int)(1200/M));
				set_tacho_speed_sp(m_left_tacho, (int)(-0.15*speed));
				set_tacho_speed_sp(m_right_tacho, (int)(0.15*speed));
				set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
				set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
				
				
				set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
				set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
				set_tacho_command_inx(m_left_tacho, TACHO_STOP);
				set_tacho_command_inx(m_right_tacho, TACHO_STOP);*/
				if(turn(((int)(-90/HALF_NB_SAMPLES)))<0){
					return -1;
				}
				
				//The robot measure distances during the maneuver.
				obs_pos_y = (int)get_distance_us();
			
				vals[k+1]=obs_pos_y;
				
				printf("k=%d\n",k);
			
			}
			
			//We look for the minimum measured distance. An object is detected if the minimum measured distance is inferior to a threshold.
			valmin=20000;
			argmin=-1;
			for(int k=0; k<2*HALF_NB_SAMPLES+1; k++){
				if(vals[k]<valmin){
					valmin = vals[k];
					argmin=k;
				}
			}
			if(valmin<400){
				
				//old code kept just in case.
				/*set_tacho_time_sp(m_left_tacho, (int)((1000/M)*(2*M-argmin)));
				set_tacho_time_sp(m_right_tacho, (int)((1000/M)*(2*M-argmin)));
				set_tacho_speed_sp(m_left_tacho, (int)((0.15)*speed));
				set_tacho_speed_sp(m_right_tacho, (int)((-0.15)*speed));
				set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
				set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
				sleep(2);
				
				set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
				set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
				set_tacho_command_inx(m_left_tacho, TACHO_STOP);
				set_tacho_command_inx(m_right_tacho, TACHO_STOP);*/
				
				//If an object is detected, the robot goes in its direction.
				if(turn((int)((90/HALF_NB_SAMPLES)*(2*HALF_NB_SAMPLES-argmin)))<0){
					return -1;
				}
				break;
			}
			
			//old code kept just in case.
			/*set_tacho_time_sp(m_left_tacho, 1000);
			set_tacho_time_sp(m_right_tacho, 1000);
			set_tacho_speed_sp(m_left_tacho, (int)(0.15*speed));
			set_tacho_speed_sp(m_right_tacho, (int)(-0.15*speed));
			set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
			set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
			
			sleep(1);
			
			set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
			set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
			set_tacho_command_inx(m_left_tacho, TACHO_STOP);
			set_tacho_command_inx(m_right_tacho, TACHO_STOP);*/
			if(turn(90)<0){
				return -1;
			}
			
			set_tacho_time_sp(m_left_tacho, (int)(1000));
			set_tacho_time_sp(m_right_tacho, (int)(1000));
			set_tacho_speed_sp(m_left_tacho, (int)(relative_speed*max_speed_tacho));
			set_tacho_speed_sp(m_right_tacho, (int)(relative_speed*max_speed_tacho));
			set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
			set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
			sleep(1);
			
			set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
			set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
			set_tacho_command_inx(m_left_tacho, TACHO_STOP);
			set_tacho_command_inx(m_right_tacho, TACHO_STOP);
			
		}while(true);

		//As the robot get closer from the cube, it slows down.
		relative_speed=0.15;
		if(valmin<300){
			relative_speed=0.10;
		}
		if(valmin<200){
			relative_speed=0.05;
		}
		set_tacho_time_sp(m_left_tacho, (int)(1000));
		set_tacho_time_sp(m_right_tacho, (int)(1000));
		set_tacho_speed_sp(m_left_tacho, (int)(relative_speed*max_speed_tacho));
		set_tacho_speed_sp(m_right_tacho, (int)(relative_speed*max_speed_tacho));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		
		int l = 0;
		while(valmin>100 && l<1000){
			usleep(1);
			valmin = (int)get_distance_us();
			l++;
		}
	
	}while(valmin>100);
	
	set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);
		
	return 0;
}



