#include "fisher2020_motion.h"

#include "ev3.h"
#include "ev3_tacho.h"

#define SPEED_FACTOR speed_factor	//0.15
#define TIME_FACTOR 10

/* The robot should go forward of an approximate distance of <distance_from>.
 * May be mainly used to go backward actually.
 */
int forward_of(int distance_from, float speed_factor){

	if(SPEED_FACTOR<0 || SPEED_FACTOR>1){
		return -1;
	}
	
	int direction = 1;
	
	if(distance_from<0){
		direction = -1;
		distance_from = - distance_from;
	}
	
	set_tacho_time_sp(m_left_tacho, (int)(TIME_FACTOR*distance_from));
	set_tacho_time_sp(m_right_tacho, (int)(TIME_FACTOR*distance_from));
	set_tacho_speed_sp(m_left_tacho, (int)(direction*SPEED_FACTOR*max_speed_tacho));
	set_tacho_speed_sp(m_right_tacho, (int)(direction*SPEED_FACTOR*max_speed_tacho));
	set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
	set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
	
	//TODO sleep?
	
	return 1;

}

