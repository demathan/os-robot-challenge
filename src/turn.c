#include "fisher2020_motion.h"

#include "fisher2020_sensors.h"

#include "ev3.h"
#include "ev3_tacho.h"

#define SPEED_FACTOR 0.05

/*The robot turn of a given angle in degrees in clockwise direction and use the gyroscope to be precised*/

int turn(int degrees){

	int direction = 1;
	int sign = 1;
	if(degrees<0){
		direction=-1;
		sign=-1;
	}

	int original = get_angle_gyro();
        
	set_tacho_speed_sp(m_left_tacho, (int)(direction*SPEED_FACTOR*max_speed_tacho));
	set_tacho_speed_sp(m_right_tacho, (int)(direction*-SPEED_FACTOR*max_speed_tacho));
	set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
	set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
        
        int value_gyro = original;
        do{
		value_gyro = (int) get_angle_gyro();
	}while(sign*(value_gyro-original)<sign*degrees);
    
        set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);
         
        return 0;
}
