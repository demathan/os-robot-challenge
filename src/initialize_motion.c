#include "fisher2020_motion.h"

#include "fisher2020_sensors.h"

#include <stdio.h>

#include "ev3.h"
#include "ev3_tacho.h"

#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_wrench_tacho 65

int initialize_motion(){

	if(ev3_tacho_init()<1){
		return -1;
	}

	//initialize the motors
	int ret_left = ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
	int ret_right = ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
	int ret_crane = ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
	int ret_wrench = ev3_search_tacho_plugged_in(port_wrench_tacho,0,&m_wrench_tacho,0);
	if(ret_left<1 || ret_right<1 || ret_crane<1 || ret_wrench<1 ){
		return -1;
	}
	
	//initialize max_speed_tacho
	int max_speed_left_tacho = 0;
	int max_speed_right_tacho = 0;
	get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
 	get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);
 	max_speed_tacho = max_speed_right_tacho > max_speed_left_tacho ? max_speed_left_tacho : max_speed_right_tacho;
 	
 	//initialize max_wrench_speed
 	get_tacho_max_speed(m_wrench_tacho,&max_wrench_speed);
 	
 	//initialize wrench_state
 	wrench_state = 0;
 	
 	gyro_target=get_angle_gyro();
 	
 	printf("initialize motors:\nmax_speed_tacho = %d\nmax_wrench_speed = %d\n",max_speed_tacho,max_wrench_speed);
 	
 	return 0;

}

