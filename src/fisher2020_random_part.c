#include "fisher2020.h"
#include "fisher2020_motion.h"

#define NB_BALLS 4

#define BACKWARD_DISTANCE1 1000

#define BACKWARD_DISTANCE2 100

#define BACKWARD_DISTANCE3 150

#define FORWARD_DISTANCE 300

#define WALL_DISTANCE 600

#define DEST_CUBE_WALL_DISTANCE 400

#define DEST_CUBE_DISTANCE 35

#define TEST_FISHER2020_RANDOM_PART

#ifdef TEST_FISHER2020_RANDOM_PART

int main(int argc, char * argv[]){
	
	if(initialization()<0){
		return -1;
	}
	return fisher2020_random_part();
	
}

#endif

/* This function performs the second part of the challenge.
 */
int fisher2020_random_part(){

	int status=0;
	
	//The robot starts from the middle of the arena (800 cm from the y- wall) and goes to it's starting position.
	if(random_cube_search()<0){
		return -1;
	}
	
	if(forward_of(-BACKWARD_DISTANCE1,0.20f)<0){
		return -1;
	}
	sleep(10);
	
	for(int i=0; i<NB_BALLS; i++){
		
		if(forward_to(random_origin_cube_distance+100,0.20f)<0){
			return -1;
		}
		
		if(forward_to(random_origin_cube_distance,0.05f)<0){
			return -1;
		}
		
		//turn to the side of the cube
		if(random_origin_cube_side==RANDOM_CUBE_ON_RIGHT_SIDE){
			turn(-90);
		}else{
			turn(90);
		}
		
		//go to the box
		if(forward_to(35,0.05f)<0){
			return -1;
		}
		
		//Try to grab a ball until grabing one.
		if(forward_of(50,0.05)<0){
			return -1;
		}
		sleep(1);
		status=0;
		if(crane_take()<0){
			return -1;
		}
		while(is_there_something_in_the_wrench()==0){
		
			//status represents different positions that the robot tries to grab a ball.
			status=status+1;
			if(status==1||status==6||status==4||status==9){
				if(turn(5)<0){
					return -1;
				}
			}
			if(status==2||status==7||status==3||status==8){
				if(turn(-5)<0){
					return -1;
				}
			}
			if(status==5){
				if(forward_of(30,0.05)<0){
					return -1;
				}
				sleep(1);
			}
			
			if(open_wrench()<0){
				return -1;
			}
			
			if(status==10){
				//give up
				break;
			}
			
			if(crane_take()<0){
				return -1;
			}
			
		}
		//Exiting the while loop means that a ball is in the wrench or that we gave up.
		
		if(status==10){
			//we gave up
			return 0;
		}
		
		//Position correction before going
		if(status==1||status==6||status==4||status==9){
			if(turn(-5)<0){
				return -1;
			}
		}
		if(status==2||status==7||status==3||status==8){
			if(turn(5)<0){
				return -1;
			}
		}
		
		if(forward_of(-BACKWARD_DISTANCE2,0.20f)<0){
			return -1;
		}
		sleep(1);
		
		//turn to the opposite side.
		if(random_origin_cube_side==RANDOM_CUBE_ON_RIGHT_SIDE){
			turn(180);
		}else{
			turn(-180);
		}
		
		if(forward_to(WALL_DISTANCE,0.20f)<0){
			return -1;
		}
		
		//turn to y+ direction.
		if(random_origin_cube_side==RANDOM_CUBE_ON_RIGHT_SIDE){
			turn(90);
		}else{
			turn(-90);
		}
		
		//move to 40 cm from the y+ wall.
		if(forward_to(DEST_CUBE_WALL_DISTANCE+100,0.20f)<0){
			return -1;
		}
		
		if(forward_to(DEST_CUBE_WALL_DISTANCE,0.05f)<0){
			return -1;
		}
		
		//Turn to face the cube.
		if(turn(-90)<0){
			return -1;
		}
		
		//go to the box
		if(forward_to(35,0.05f)<0){
			return -1;
		}
		
		//Drop the ball.
		if(crane_drop()<0){
			return -1;
		}
		
		//If it's the fouth ball, we can stop here.
		if(i==NB_BALLS-1){
			break;
		}
		
		if(forward_of(-BACKWARD_DISTANCE3,0.20f)<0){
			return -1;
		}
		sleep(2);
		
		//Turn to go back.
		if(turn(-90)<0){
			return -1;
		}
		
		if(forward_of(FORWARD_DISTANCE,0.20f)<0){
			return -1;
		}
		sleep(3);
		
	}
	
	return 1;
	
}

