#include "fisher2020.h"

int main(int argc, char * argv[]){
	
	if(initialization()<0){
		return -1;
	}
	
	int ret = fisher2020_deterministic_part();
	if(ret == 1){
		if(pyramid_to_start_search_position()<0){
			return -1;
		}
	}else if(ret == 0){
		if(cube_to_start_search_position()<0){
			return -1;
		}
	}else{
		return -1;
	}
	
	if(fisher2020_random_part()<0){
		return -1;
	}
	
	return 0;
	
}

