#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"

#include "fisher2020.h"

#define BACK_DISTANCE 150

#define FORWARD_DISTANCE1 600



/* This function drives the robot from the known origin cube to the search start position.
 */
int cube_to_start_search_position(){
	
	//First go backward.
	if(forward_of(-BACK_DISTANCE,0.20f)<0){
		return -1;
	}
	sleep(2);
	
	//Then turn right.
	if(turn(90)<0){
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
		return -1;
	}
	
	if(forward_of(FORWARD_DISTANCE,0.20f)<0){
		return -1;
	}
	sleep(3);
	
	return 0;
}

#ifdef TEST_PYRAMID_TO_START_POS

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	return pyd_to_start_pos();
}

#endif


