#ifndef FISHER2020

#define FISHER2020

//Variables declaration

float o_compass;

int random_origin_cube_side;
#define RANDOM_CUBE_ON_RIGHT_SIDE 1
#define RANDOM_CUBE_ON_LEFT_SIDE 2
int random_origin_cube_distance;

//Functions declaration

/* Initilize the robot before doing anything else.
 */
int initialization();

/* A function that uses the compass to correct the angular position of the robot.
 */
int angular_position_correction(float o_compass_local, int wished);

/* Returns 0 if there is nothing in the wrench and 1 if there is something.
 */
int is_there_something_in_the_wrench();

/* This function drives the robot from its original position to the known origin cube.
 */
int start_pos_to_origin_cube();

/* This function drives the robot from the known origin cube to the pyramid.
 */
int known_origin_cube_to_pyramid();

/* This function drives the robot from the known origin to the pyramid.
 */
int pyramid_to_origin_cube();

/* This function searches the randomly placed cube (starting from the start position of the robot).
 */
int random_cube_search();

/* Pushes the randomly placed cube to a known position.
 */
int push_the_cube();	//deprecated

/* This function drives the robot from the known origin cube to the search start position.
 */
int cube_to_start_search_position();

/* This function drives the robot from the pyramid to the search start position.
 */
int pyramid_to_start_search_position();

/* This function performs the first part of the challenge.
 */
int fisher2020_deterministic_part();

/* This function performs the second part of the challenge.
 */
int fisher2020_random_part();

#endif

