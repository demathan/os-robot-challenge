
#include "fisher2020_motion.h"

#include "fisher2020_sensors.h"

#include "ev3.h"
#include "ev3_tacho.h"
#include "ev3_servo.h"

#include<unistd.h>
//uint8_t m_wrench_tacho;



#define TIME_FACTOR_WRENCH 400
#define SPEED_FACTOR_WRENCH 0.125
int close_wrench(){

    
    set_tacho_time_sp( m_wrench_tacho, TIME_FACTOR_WRENCH );    //tacho tuns for 500 ms
    set_tacho_speed_sp(m_wrench_tacho, (int)(SPEED_FACTOR_WRENCH*max_wrench_speed));
    set_tacho_command_inx(m_wrench_tacho, TACHO_RUN_TIMED);

    sleep(1);
    
    set_tacho_stop_action_inx(m_wrench_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_wrench_tacho, TACHO_STOP);

    return 0;

}
