#ifndef FISHER2020_MOTION

#define FISHER2020_MOTION


#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"

#include <stdlib.h>

//Variables declaration

//motor interfaces
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t m_wrench_tacho;

//Other variables
int max_speed_tacho;
int max_wrench_speed;
int wrench_state;	//it may be useful to know if the wrench is open or closed.

int gyro_target;

//function declarations

/* Should be called before moving to initialize all the needed objects.
 */
int initialize_motion();

/* The robot should go forward until detecting an object at a distance superior or equal to <distance_to> in front of him.
 * /!\ Will require the sensor interface.
 */
int forward_to(int distance_to, float speed_factor);

/* The robot should go forward of an approximate distance of <distance_from>.
 * May be mainly used to go backward actually.
 */
int forward_of(int distance_from, float speed_factor);

/* The robot should turn of <angle> degrees in clockwise direction.
 * /!\ Will require the sensor interface.
 */
int turn(int angle);

//TODO Maybe an angle position correction function that uses the compass to correct the position of the robot.

/* The crane should go down, close the wrench and go up.
 */
int crane_take();

/* The crane should go down, open the wrench and go up. Doesn't go as low as the previous function.
 */
int crane_drop();

/* The wrench should open.
 */
int close_wrench();

/* The wrench should open.
 */
int open_wrench();

/* In case of an emergency, stops all the motors.
 */
int stop();

#endif


