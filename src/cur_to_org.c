#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"

#include "fisher2020.h"

#define FRONT_DISTANCE 1000

#define WALL_DISTANCE1 300

#define WALL_DISTANCE2 200

#define CUBE_DISTANCE1 100

#define CUBE_DISTANCE2 35

/* This function drives the robot from its original position to the known origin cube.
 */
int start_pos_to_origin_cube(){
	
	//First go forward until reaching a distance of 20cm from the wall.
	if(forward_of(FRONT_DISTANCE,0.20f)<0){
		return -1;
	}
	
	sleep(5);
	
	if(forward_to(WALL_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(WALL_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	//Then turn right.
	if(turn(90)<0){
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
		return -1;
	}
	
	//And continue forward until reaching the cube.
	if(forward_to(CUBE_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(CUBE_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	return 0;
}

#ifdef TEST_START_POS_TO_ORIGIN_CUBE

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	return start_pos_to_origin_cube();
}

#endif

