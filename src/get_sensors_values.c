#include "fisher2020_sensors.h"

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"

float get_distance_us(){

	float value;
	
	if(get_sensor_value0(sn_us,&value)>0){
		return value;
	}
	
	return -1.0f;

}

float get_angle_gyro(){

	float value;
	
	get_sensor_value0(sn_gyro,&value);
	
	return value;

}

float get_angle_compass(){

	float value;
	
	if(get_sensor_value0(sn_compass,&value)>0){
		return value;
	}
	
	return -1.0f;

}

float get_measure_color(){

	float value;
	
	if(get_sensor_value0(sn_color,&value)>0){
		return value;
	}
	
	return -1.0f;

}

