#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"

#include "fisher2020.h"

#include <unistd.h>

#define SLEEP_TIME 30

/* A function that uses the compass to correct the angular position of the robot.
 */
int angular_position_correction(float o_compass_local, int wished){

	turn(0);

	sleep(SLEEP_TIME);
	
	float current_compass = get_angle_compass();
	if(current_compass<0.0f){
		return(-1);
	}
	float current_relative = current_compass - o_compass;
	int order = (wished-((int)current_relative))%360;
	if(order>180){
		order = order-360;
	}
	
#ifdef DEBUG_ANGULAR_POSITION_CORRECTION
	printf("angular_position_correction: o_compass=%f\nwished=%d\ncurrent_compass=%f\ncurrent_relative=%f\norder=%d\n",o_compass,wished,current_compass,current_relative,order);
	
#endif
	int ret_turn = turn(order);
	if(ret_turn<0){
		return -1;
	}
	
	return 0;

}

