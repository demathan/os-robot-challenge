#include "fisher2020_motion.h"

#include "ev3.h"
#include "ev3_tacho.h"

/* In case of an emergency, stops all the motors.
 */
int stop(){

	set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);

	set_tacho_stop_action_inx(m_wrench_tacho, TACHO_HOLD);
	set_tacho_command_inx(m_wrench_tacho, TACHO_STOP);

	set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
	set_tacho_command_inx(m_crane_tacho, TACHO_STOP);
	
	return 0;

}

