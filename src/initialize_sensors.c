#include "fisher2020_sensors.h"

#include <stdlib.h>
#include <pthread.h>

/* Initialize sensors value tracking and all the needed objects.
 */
int initialize_sensors(){

#ifdef THREAD_MODE

	if(sensors_thread_active != 0){
		return -1;
	}
	sensors_thread_active = 1;

	distance_us = -1;
	angle_gyro = -1;
	angle_compass = -1;
	measure_color = -1;
	
	pthread_mutex_init(&sensors_thread_mutex, NULL);
	
	pthread_create(&sensors_thread_id,NULL,sensors_thread_function/*If the sensors thread function needs arguments, put them here*/);
	
	return 0;
	
#else

	if(ev3_sensor_init()<1){
		return -1;
	}

	int ret_us = ev3_search_sensor(LEGO_EV3_US, &sn_us, 0);
	int ret_gyro = ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0);
	int ret_compass = ev3_search_sensor(HT_NXT_COMPASS, &sn_compass, 0);
	int ret_color = ev3_search_sensor(LEGO_EV3_COLOR, &sn_color, 0);
	//TODO check if everything went well.
	
	ret_us = set_sensor_mode_inx(sn_us, LEGO_EV3_US_US_DIST_CM );
	ret_gyro = set_sensor_mode_inx(sn_gyro, LEGO_EV3_GYRO_GYRO_ANG );
	ret_compass = set_sensor_mode_inx(sn_color, LEGO_EV3_COLOR_COL_REFLECT );
	ret_color = set_sensor_mode_inx(sn_compass, HT_NXT_COMPASS_COMPASS );
	//TODO check if everything went well.
	
	return 0;

#endif

}

