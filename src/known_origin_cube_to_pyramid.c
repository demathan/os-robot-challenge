#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"

#include "fisher2020.h"

#define BACK_DISTANCE 150

#define FORWARD_DISTANCE 300

#define WALL_DISTANCE1 900

#define WALL_DISTANCE2 800

#define PYRAMID_DISTANCE1 200

#define PYRAMID_DISTANCE2 50

#ifdef TEST_KNOWN_ORIGIN_CUBE_TO_PYRAMID

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int angular_position_correction(float o_compass, int wished);

int known_origin_cube_to_pyramid();

float o_compass;

int main(int argc, char * argv[]){
	initialize_sensors();
	initialize_motion();
	o_compass=get_angle_compass();
	printf("main: o_compass=%f\n",o_compass);
	return known_origin_cube_to_pyramid();
}

#endif

/* This function drives the robot from the known origin cube to the pyramid.
 */
int known_origin_cube_to_pyramid(){
	
	//First go a bit backward.
	if(forward_of(-BACK_DISTANCE,0.20f)<0){
		return -1;
	}
	sleep(2);
	
	
	//Then turn right.
	if(turn(90)<0){	//not 90 because the robot tends to go a little to much on the right.
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
			return -1;
		}
	
	//Go forward until beingat 80cm from the wall in front.
	if(forward_to(WALL_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(WALL_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	//Turn right again.
	if(turn(90)<0){	//not 90 because the robot tends to go a little to much on the right.
		return -1;
	}
	
	if(turn(0)<0){	//It's the better way to go straight.
			return -1;
		}

	//And finally go forward until reaching the pyramid.
	if(forward_to(PYRAMID_DISTANCE1,0.20f)<0){
		return -1;
	}
	
	if(forward_to(PYRAMID_DISTANCE2,0.05f)<0){
		return -1;
	}
	
	return 0;
	
}

