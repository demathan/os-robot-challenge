#include "fisher2020.h"
#include "fisher2020_motion.h"
#include "fisher2020_sensors.h"

/* Initilize the robot before doing anything else.
 */
int initialization(){
	
	if(initialize_sensors()<0){
		return -1;
	}
	
	if(initialize_motion()<0){
		return -1;
	}
	
	if((o_compass = get_angle_compass())<0.0f){
		return -1;
	}
	
	return 0;
	
}

