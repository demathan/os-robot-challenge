#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"
#include "fisher2020.h"

#define PUSH_TO_WALL 25

#define GO_HIGH 10

#define GO_TO_WALL 10

#define PUSH_TO_CORNER 60



int push_the_cube(){
    if(get_angle_compass() < (o_compass + 180)%360){   
        forward_of(PUSH_TO_WALL, 1);
        turn(o_compass);
        forward_of(GO_HIGH, 1);
        turn(o_compass-90);
        forward_to(GO_TO_WALL, 1);
        turn((-1)*o_compass);
        forward_of(PUSH_TO_CORNER, 1);
    }else{
        forward_of(PUSH_TO_WALL, 1);
        turn(o_compass);
        forward_of(GO_HIGH, 1);
        turn(o_compass+90);
        forward_to(GO_TO_WALL, 1);
        turn((-1)*o_compass);
        forward_of(PUSH_TO_CORNER, 1);
    }
    


    return 1;
}
