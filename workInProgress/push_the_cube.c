#include "fisher2020_sensors.h"
#include "fisher2020_motion.h"
#include "fisher2020.h"

#define PUSH_TO_WALL 25

#define GO_HIGH 10

#define GO_TO_WALL 35

#define PUSH_TO_CORNER 60



int push_the_cube(){
    if(get_angle_compass() < (o_compass + 180)%360){   
        angular_position_correction(o_compass, 90);
        forward_of(PUSH_TO_WALL, 0.2);
        turn(90);
        forward_of(GO_HIGH, 0.2);
        turn(-90);
        forward_to(GO_TO_WALL, 0.05);
        turn(-90);
        forward_of(PUSH_TO_CORNER, 0.2);
    }else{
        angular_position_correction(o_compass, 270);
        forward_of(PUSH_TO_WALL, 0.2);
        turn(-90);
        forward_of(GO_HIGH, 0.2);
        turn(90);
        forward_to(GO_TO_WALL, 0.05);
        turn(90);
        forward_of(PUSH_TO_CORNER, 0.2);
    }
    


    return 1;
}

