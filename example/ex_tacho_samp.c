//#include "motion.h"	//unknown
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;

int max_speed_left_tacho;
int max_speed_right_tacho;

//author : Prithiraj unlimited movement
//initialize motors
int main(void)
{
	
     //detection of tacho motors
    int i;
    char s[256];
    
    int speed;
    // uncmment these lines if compilation fails
    //ev3_init(); 
    //detection of tacho motors
    while(ev3_tacho_init()<1)
        sleep(1);
    printf("Tacho motor found\n");
    printf("Found tacho motors:\n");
  for(i=0;i<DESC_LIMIT;i++){
    if(ev3_tacho[ i ].type_inx!=TACHO_TYPE__NONE_){
      printf("  type = %s\n",ev3_tacho_type(ev3_tacho[i].type_inx));
      printf("  port = %s\n",ev3_tacho_port_name(i,s));
      printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
    }
  }
	// comment out the below lines if needed for testing purposes
    ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
    ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
    ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
    get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
    get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);
 
    printf("max_speed_right_tacho = %d\n", max_speed_right_tacho);
    printf("max_speed_left_tacho = %d\n", max_speed_left_tacho);


    speed = max_speed_right_tacho>max_speed_left_tacho?max_speed_left_tacho: max_speed_right_tacho;


    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP);

//move forward unlimited

    
    set_tacho_speed_sp(m_left_tacho, (int)(0.75*speed));
    set_tacho_speed_sp(m_right_tacho, (int)(0.75*speed));
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
    //return 0; //NO!!!
    sleep(5);



// reverse speed
    /*
    speed = -speed;
    set_tacho_speed_sp(m_left_tacho, speed);
    set_tacho_speed_sp(m_right_tacho, speed);
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);*/
    

//brake

    set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_left_tacho, TACHO_STOP);
    set_tacho_command_inx(m_right_tacho, TACHO_STOP);
    return 0;
    
}





