#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68

#define N 10000
#define M 3

FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

	int i,speed;
	char s[256];
	uint8_t sn_us;
	int val;
	int unit = 128;
	int valmin;
	int argmin;
	float relative_speed = 0.25;

	while ( ev3_tacho_init() < 1 ) 
	     Sleep( 1000 );                             //Detect the motors
	printf( "motors work:\n" );
	ev3_sensor_init();                             //detection of sensors
	printf("Sensors found\n");
 

	for ( i = 0; i < DESC_LIMIT; i++ ) {
	    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
	        printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
	        printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
	        printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
	    }
	}

	if ( ev3_search_sensor( LEGO_EV3_US, &sn_us, 0 )) {   //Look at the sensor ultrasonic
	    printf( "IR sensor is found\n" );
	    
	    if ( get_sensor_value( 0, sn_us, &val ) /*&& get_sensor_unit(0, sn_us, &unit)*/) {        //Gives the value returned by the US sensor
	        printf( "US sensor value = %d %c\n", val, unit );
	    }else{
	        printf("error getting the sensor value\n");
	    }
	} else {
	    printf( "US sensor is NOT found\n" );
	}
	   
	// comment out the below lines if needed for testing purposes
	ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
	ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
	ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
	get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
	get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);

	speed = max_speed_right_tacho>max_speed_left_tacho?max_speed_left_tacho: max_speed_right_tacho;
	
	set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
	set_tacho_command_inx(m_crane_tacho, TACHO_STOP);
	
	i=1;
	int vals[ 2 * M ];
	
	do{
	
		do{
	
		set_tacho_time_sp(m_left_tacho, 1000);
		set_tacho_time_sp(m_right_tacho, 1000);
			set_tacho_speed_sp(m_left_tacho, (int)(0.15*speed));
		set_tacho_speed_sp(m_right_tacho, (int)(-0.15*speed));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		sleep(1);
		
		set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
		set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
		set_tacho_command_inx(m_left_tacho, TACHO_STOP);
		set_tacho_command_inx(m_right_tacho, TACHO_STOP);
		
		for(int k = 0; k<N; k++){
			get_sensor_value(i, sn_us, &val);
			i++;
		}

		vals[0]=val;
		
		
		for(int k =0; k< 2 * M ; k++){
		
			set_tacho_time_sp(m_left_tacho, (int)(1200/M));
			set_tacho_time_sp(m_right_tacho, (int)(1200/M));
			set_tacho_speed_sp(m_left_tacho, (int)(-0.15*speed));
			set_tacho_speed_sp(m_right_tacho, (int)(0.15*speed));
			set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
			set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
			sleep(1);
			
			set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
			set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
			set_tacho_command_inx(m_left_tacho, TACHO_STOP);
			set_tacho_command_inx(m_right_tacho, TACHO_STOP);
			for(int k = 0; k<N; k++){
				get_sensor_value(i, sn_us, &val);
				i++;
			}
		
			vals[k+1]=val;
		
		}
		
		valmin=20000;
		argmin=-1;
		for(int k=0; k<2*M+1; k++){
			if(vals[k]<valmin){
				valmin = vals[k];
				argmin=k;
			}
		}
		if(valmin<400){
			
			set_tacho_time_sp(m_left_tacho, (int)((1000/M)*(2*M-argmin)));
			set_tacho_time_sp(m_right_tacho, (int)((1000/M)*(2*M-argmin)));
			set_tacho_speed_sp(m_left_tacho, (int)((0.15)*speed));
			set_tacho_speed_sp(m_right_tacho, (int)((-0.15)*speed));
			set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
			set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
			sleep(2);
			
			set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
			set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
			set_tacho_command_inx(m_left_tacho, TACHO_STOP);
			set_tacho_command_inx(m_right_tacho, TACHO_STOP);
			
			break;
		}
		
		set_tacho_time_sp(m_left_tacho, 1000);
		set_tacho_time_sp(m_right_tacho, 1000);
		set_tacho_speed_sp(m_left_tacho, (int)(0.15*speed));
		set_tacho_speed_sp(m_right_tacho, (int)(-0.15*speed));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		sleep(1);
		
		set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
		set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
		set_tacho_command_inx(m_left_tacho, TACHO_STOP);
		set_tacho_command_inx(m_right_tacho, TACHO_STOP);
		
		set_tacho_time_sp(m_left_tacho, (int)(1000));
		set_tacho_time_sp(m_right_tacho, (int)(1000));
		set_tacho_speed_sp(m_left_tacho, (int)(relative_speed*speed));
		set_tacho_speed_sp(m_right_tacho, (int)(relative_speed*speed));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		sleep(1);
		
		set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
		set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
		set_tacho_command_inx(m_left_tacho, TACHO_STOP);
		set_tacho_command_inx(m_right_tacho, TACHO_STOP);
		
		}while(true);

		//move forward limited
		relative_speed=0.15;
		if(valmin<300){
			relative_speed=0.10;
		}
		if(valmin<200){
			relative_speed=0.05;
		}
		set_tacho_time_sp(m_left_tacho, (int)(1000));
		set_tacho_time_sp(m_right_tacho, (int)(1000));
		set_tacho_speed_sp(m_left_tacho, (int)(relative_speed*speed));
		set_tacho_speed_sp(m_right_tacho, (int)(relative_speed*speed));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		//sleep(1);
		for(int k = 0; k<N; k++){
			usleep(1);
			get_sensor_value(i, sn_us, &val);
			i++;
		}
	
	}while(valmin>100);	

	set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);

	ev3_uninit();
	return 0;
}
