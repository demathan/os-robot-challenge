//get_sensor_value0(sn2, &value_gyro);
//get_sensor_value1(sn2, &value_gyro);


#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_servo.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_gyro;
uint8_t sn2;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i;
    char s[256];
    int speed=0;
    //uint8_t sn_gyro;
    int val;
    int unit = 128;
    float value_gyro0,value_gyro1;
    
    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_port_init();
    ev3_servo_init();
    
    printf("servo works\n");
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");
 

	for ( i = 0; i < DESC_LIMIT; i++ ) {
	    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
		printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
		printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
		printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
	    }
	}

	if ( ev3_search_sensor( LEGO_EV3_GYRO, &sn_gyro, 0 )) {   //Look at the sensor compass
		printf( "IR sensor is found\n" );
		
		if ( get_sensor_value( 0, sn_gyro, &val ) /*&& get_sensor_unit(0, sn_compass, &unit)*/) {        //Gives the value returned by the COMPASS sensor
		    printf( "COMPASS sensor value = %d %c\n", val, unit );
		}else{
		    printf("error getting the sensor value\n");
		}
	    } else {
		printf( "COMPASS sensor is NOT found\n" );
	    }


	ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
	ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
	get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
	get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);



	speed = max_speed_right_tacho>max_speed_left_tacho?max_speed_left_tacho: max_speed_right_tacho;	
	//set_tacho_time_sp(m_left_tacho, 2000); 
	//set_tacho_time_sp(m_right_tacho, 2000);   
	set_tacho_speed_sp(m_left_tacho, 0.05*speed);
	set_tacho_speed_sp(m_right_tacho, 0.05*(-speed));
	set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
	set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
	i=1;
	set_sensor_mode_inx(sn_gyro, LEGO_EV3_GYRO_GYRO_G_AND_A );

	get_sensor_value0(sn_gyro, &value_gyro0);
	printf("gyro0:%f\n",value_gyro0);
	get_sensor_value1(sn_gyro, &value_gyro1);
	printf("gyro1:%f\n",value_gyro1);
	i++;
	
	float original = value_gyro0;

	int ret=100000;
	do{
		
		get_sensor_value0(sn_gyro, &value_gyro0);
		printf("gyro0:%f\n",value_gyro0);
		get_sensor_value1(sn_gyro, &value_gyro1);
		printf("gyro1:%f\n",value_gyro1);
	    	i++;
		ret--;
		
	}while(ret>0 && value_gyro0-original<90);
	
	set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
	set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
	
	set_tacho_command_inx(m_left_tacho, TACHO_STOP);
	set_tacho_command_inx(m_right_tacho, TACHO_STOP);
	
	ev3_uninit();
	return 0;

}

