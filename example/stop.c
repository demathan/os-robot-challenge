#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_s;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i;
    char s[256];
    uint8_t sn_g;
    int val;
    int unit = 128;

    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");

    ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
    ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
    ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
    int ret=ev3_search_tacho_plugged_in(port_servo, 0, &sn_s, 0 );
    if(ret==0){printf( "So called Servo motor is found, setting position...\n" );} 

    set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
    
    set_tacho_command_inx(m_left_tacho, TACHO_STOP);
    set_tacho_command_inx(m_right_tacho, TACHO_STOP);
    
    set_tacho_stop_action_inx(sn_s, TACHO_HOLD);
    set_tacho_command_inx(sn_s, TACHO_STOP);

    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP);

    ev3_uninit();
    return 0;
}