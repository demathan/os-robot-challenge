#include <string.h>
#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

/*
API : http://in4lio.github.io/ev3dev-c/group__ev3__sensor.html#ga0ab92bd15fee7bdf289f613f010b8475
*/

int main(void)
{
    char s[ 256 ];
    int val;
    uint32_t n, i, ii;
    uint8_t sn_us;

    ev3_sensor_init(); //detection of sensors
    printf("Sensors found\n");
 
    int unit=45;
    for ( i = 0; i < DESC_LIMIT; i++ ) {   //Find all the sensors and give information
        if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
            printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
            if ( get_sensor_mode( i, s, sizeof( s ))) {
                printf( "  mode = %s\n", s );
            }
            if ( get_sensor_num_values( i, &n )) {
                for ( ii = 0; ii < n; ii++ ) {
                    if ( get_sensor_value( ii, i, &val )/*&& get_sensor_unit(0, sn_us, &unit)*/) {        //Gives the value returned by the US sensor
                        printf( "US sensor value = %d %c\n", val, unit );
                    }
                }
            }
        }
    }

    if ( ev3_search_sensor( LEGO_EV3_US, &sn_us, 0 )) {   //Look at the sensor ultrasonic
        printf( "IR sensor is found\n" );
        if ( get_sensor_value( 0, sn_us, &val )) {        //Gives the value returned by the US sensor
            printf( "US sensor value = %d\n", val );
        }else{
            printf("error getting the sensor value\n");
        }
    } else {
        printf( "US sensor is NOT found\n" );
    }
 
    ev3_uninit();
    printf("terminated\n");
     
    return 0;

}
