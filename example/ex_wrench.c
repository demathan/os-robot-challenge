#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_servo.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_s;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i;
    char s[256];
    
    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_port_init();
    ev3_servo_init();
    
    printf("servo works\n");
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");
 

    for ( i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
            printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
            printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
        }
    }
    
	// comment out the below lines if needed for testing purposes
    ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
    ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
    ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
    get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
    get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);
    
    set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_left_tacho, TACHO_STOP);
    set_tacho_command_inx(m_right_tacho, TACHO_STOP);
    
    for ( i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_servo[ i ].type_inx != SERVO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_servo_type( ev3_servo[ i ].type_inx ));
            printf( "  port = %s\n", ev3_servo_port_name( i, s ));
        }
    }
    if ( ev3_search_servo_plugged_in(port_servo, 0, &sn_s, 0 )) {
        printf( "Servo motor is found, setting position...\n" );
        set_servo_position_sp( sn_s, 100 );          //Values are from -100 minimum (counter-clockwise) position to 100 maximum (clockwise) position           
        set_servo_command_inx( sn_s, SERVO_RUN );
        set_servo_command_inx( sn_s, SERVO_FLOAT );
        sleep(5000);
        set_servo_position_sp( sn_s, 0 );
        set_servo_command_inx( sn_s, SERVO_RUN );
        set_servo_command_inx( sn_s, SERVO_FLOAT );
        sleep(5000);
        set_servo_position_sp( sn_s, -100 );
        set_servo_command_inx( sn_s, SERVO_RUN );
        set_servo_command_inx( sn_s, SERVO_FLOAT );
    } else {
        printf( "Servo motor is NOT found\n" );
    }

    ev3_uninit();
    return 0;
    
}

