#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_servo.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_s;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i;
    char s[256];
    int speed=0;
    uint8_t sn_compass;
    int val;
    int val0;
    int val1;
    char mode[128];
    int unit = 128;
    int ret = 0;
    
    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_port_init();
    ev3_servo_init();
    
    printf("servo works\n");
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");
 

	for ( i = 0; i < DESC_LIMIT; i++ ) {
	    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
		printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
		printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
		printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
	    }
	}

	if ( ev3_search_sensor( HT_NXT_GYRO/*LEGO_EV3_GYRO*/, &sn_gyro, 0 )) {   //Look at the sensor gyro
		printf( "IR sensor is found\n" );
                
                if (set_sensor_mode_inx(sn_gyro, LEGO_EV3_GYRO_GYRO_G_AND_A){
                    get_sensor_mode_inx(sn_gyro, &mode);
                    printf("GYRO sensor mode = %c\n", mode); 
                }else{
                    printf("error getting the sensor value\n");
                }
		
		if ( get_sensor_value0( 0, sn_gyro, &val0 ) && get_sensor_value0( 0, sn_gyro, &val1 ) /*&& get_sensor_unit(0, sn_gyro, &unit)*/) {        //Gives the value returned by the GYRO sensor
		    printf( "GYRO sensor value = %d %c\n", val0, unit );
                    printf( "GYRO sensor value = %d %c\n", val1, unit );
		}else{
		    printf("error getting the sensor value\n");
		}
	    } else {
		printf( "GYRO sensor is NOT found\n" );
	    }
    
	ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
	ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
	get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
	get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);



	speed = max_speed_right_tacho>max_speed_left_tacho?max_speed_left_tacho: max_speed_right_tacho;	
	for(int j=0; j<10; j++){
		set_tacho_time_sp(m_left_tacho, 1000); 
		set_tacho_time_sp(m_right_tacho, 1000);   
		set_tacho_speed_sp(m_left_tacho, (int)(0.05*speed));
		set_tacho_speed_sp(m_right_tacho, (int)(0.05*(-speed)));
		set_tacho_command_inx(m_left_tacho, TACHO_RUN_TIMED);
		set_tacho_command_inx(m_right_tacho, TACHO_RUN_TIMED);
		sleep(1);
		i=1;
		ret=10000;
		do{
			get_sensor_value0(i, sn_gyro, &val0);
                        get_sensor_value1(i, sn_gyro, &val1);
		    	i++;
			ret--;
		}while(ret>0);
		printf(" GYRO ANGLE val is %d \n",val0);
                printf(" GYRO RATE val is %d \n",val1);
	}
	ev3_uninit();
	return 0;

}

