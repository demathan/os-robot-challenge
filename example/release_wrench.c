#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_servo.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_s;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i;
    char s[256];
    int speed=0;
    uint8_t sn_g;
    int val;
    int unit = 128;
    
    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_port_init();
    ev3_servo_init();
    
    printf("servo works\n");
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");
 

    for ( i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
            printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
            printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
        }
    }
   
    int ret=ev3_search_tacho_plugged_in(port_servo, 0, &sn_s, 0 );
    if(ret==0){printf( "So called Servo motor is found, setting position...\n" );} 
	// comment out the below lines if needed for testing purposes
    ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
    ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
    ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
    get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
    get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);
    get_tacho_max_speed(sn_s,&speed);
    
    set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_left_tacho, TACHO_STOP);
    set_tacho_command_inx(m_right_tacho, TACHO_STOP);
    
    set_tacho_stop_action_inx(sn_s, TACHO_HOLD);
    set_tacho_command_inx(sn_s, TACHO_STOP);
    
    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP);
    
    sleep(1);
    
 
   
    set_tacho_time_sp(m_crane_tacho, 800 ); 
    set_tacho_speed_sp(m_crane_tacho, -100);
    set_tacho_command_inx(m_crane_tacho, TACHO_RUN_TIMED);
    
    sleep(1);
    
    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP);
    
    set_tacho_time_sp( sn_s, 400 );    //tacho tuns for 500 ms
    set_tacho_speed_sp(sn_s, (int)(-0.125*speed));
    set_tacho_command_inx(sn_s, TACHO_RUN_TIMED);

    sleep(1);
    
    set_tacho_stop_action_inx(sn_s, TACHO_HOLD);
    set_tacho_command_inx(sn_s, TACHO_STOP);

    set_tacho_time_sp(m_crane_tacho, 1000 ); 
    set_tacho_speed_sp(m_crane_tacho, 100);
    set_tacho_command_inx(m_crane_tacho, TACHO_RUN_TIMED);

    sleep(1);

    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP); 

    ev3_uninit();
    return 0;
    
}

