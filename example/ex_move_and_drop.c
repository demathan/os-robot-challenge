#include <stdio.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Sleep( msec ) usleep(( msec ) * 1000 )
#define port_right_tacho 67
#define port_left_tacho 66
#define port_crane_tacho 68
#define port_servo 65
FLAGS_T state;
uint8_t m_left_tacho;
uint8_t m_right_tacho;
uint8_t m_crane_tacho;
uint8_t sn_s;

int max_speed_left_tacho;
int max_speed_right_tacho;

int main( void )
{

    int i,speed;
    char s[256];
    uint8_t sn_us;
    int val;
    int unit = 128;

    while ( ev3_tacho_init() < 1 ) 
         Sleep( 1000 );                             //Detect the motors
    printf( "motors work:\n" );
    ev3_sensor_init();                             //detection of sensors
    printf("Sensors found\n");
 

    for ( i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
            printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
            printf("  port = %d %d\n",ev3_tacho_desc_port(i),ev3_tacho_desc_extport(i));
        }
    }

    if ( ev3_search_sensor( LEGO_EV3_US, &sn_us, 0 )) {   //Look at the sensor ultrasonic
        printf( "IR sensor is found\n" );
        
        if ( get_sensor_value( 0, sn_us, &val ) /*&& get_sensor_unit(0, sn_us, &unit)*/) {        //Gives the value returned by the US sensor
            printf( "US sensor value = %d %c\n", val, unit );
        }else{
            printf("error getting the sensor value\n");
        }
    } else {
        printf( "US sensor is NOT found\n" );
    }
    
    int ret=ev3_search_tacho_plugged_in(port_servo, 0, &sn_s, 0 );
    if(ret==0){printf( "So called Servo motor is found, setting position...\n" );} 
    
	// comment out the below lines if needed for testing purposes
    ev3_search_tacho_plugged_in(port_left_tacho,0,&m_left_tacho,0);
    ev3_search_tacho_plugged_in(port_right_tacho,0,&m_right_tacho,0);
    ev3_search_tacho_plugged_in(port_crane_tacho,0,&m_crane_tacho,0);
    get_tacho_max_speed(m_left_tacho,&max_speed_left_tacho);
    get_tacho_max_speed(m_right_tacho,&max_speed_right_tacho);

    speed = max_speed_right_tacho>max_speed_left_tacho?max_speed_left_tacho: max_speed_right_tacho;
    
    set_tacho_stop_action_inx(sn_s, TACHO_HOLD);
    set_tacho_command_inx(sn_s, TACHO_STOP);
    
    set_tacho_stop_action_inx(m_crane_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_crane_tacho, TACHO_STOP);

//move forward limited
   
    set_tacho_speed_sp(m_left_tacho, (int)(0.2*speed));
    set_tacho_speed_sp(m_right_tacho, (int)(0.2*speed));
    /*                                                //Uncomment these functions to see what they do
    set_tacho_ramp_up_sp(m_left_tacho, speed/(4*160) );     //motor speed will increase from 0 to 100% of maxspeed during the time you enter 
    set_tacho_ramp_up_sp(m_right_tacho, speed/(4*160) );
    set_tacho_ramp_down_sp(m_left_tacho, speed/(4*160));     //motor speed will decrease from 0 to 100% of maxspeed during the time you enter
    set_tacho_ramp_down_sp(m_right_tacho, speed/(4*160));
    set_tacho_time_sp(m_left_tacho, speed/(2*160));     //Here you indicates for how long you want the motors to run
    set_tacho_time_sp(m_right_tacho, speed/(2*160));
    */
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
    
    i=1;
    do{
    	get_sensor_value(i, sn_us, &val);
    	i++;
    }while(val>300);
    
    set_tacho_speed_sp(m_left_tacho, (int)(0.15*speed));
    set_tacho_speed_sp(m_right_tacho, (int)(-0.15*speed));
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);
    sleep(1);
    
    set_tacho_speed_sp(m_left_tacho, (int)(0.2*speed));
    set_tacho_speed_sp(m_right_tacho, (int)(0.2*speed));
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_FOREVER);


    do{
    	get_sensor_value(i, sn_us, &val);
    	i++;
    }while(val<400);
    
    do{
    	get_sensor_value(i, sn_us, &val);
    	i++;
    }while(val>50);
    
//turn
/*
    set_tacho_speed_sp(m_left_tacho, speed);
    set_tacho_speed_sp(m_right_tacho, speed);
    set_tacho_position_sp(m_left_tacho, 90 );           //We need to know what exactly is turning to 90
    set_tacho_position_sp(m_right_tacho, 90 );             //To turn use counts per rot ?
    set_tacho_command_inx(m_left_tacho, TACHO_RUN_TO_REL_POS );
    set_tacho_command_inx(m_right_tacho, TACHO_RUN_TO_REL_POS );
    */

//brake

    set_tacho_stop_action_inx(m_left_tacho, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right_tacho, TACHO_HOLD);
    set_tacho_command_inx(m_left_tacho, TACHO_STOP);
    set_tacho_command_inx(m_right_tacho, TACHO_STOP);
    
    set_tacho_time_sp( sn_s, 400 );    //tacho tuns for 500 ms
    set_tacho_speed_sp(sn_s, (int)(-0.125*speed));
    set_tacho_command_inx(sn_s, TACHO_RUN_TIMED);

    sleep(1);
    
    set_tacho_stop_action_inx(sn_s, TACHO_HOLD);
    set_tacho_command_inx(sn_s, TACHO_STOP);

    ev3_uninit();
    return 0;
}
