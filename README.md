# OS 2020 Robot Challenge: Group 3

## Our Group

We are Group number 3. Our group is composed of

* Romain Beurdouche

* Prithiraj Das

* Jacques de Mathan

## Our Robot

Our robot is named `Fisher 2020`.

## Compile our Code

0. The compilation should be performed on a machine on which you can run the ev3dev cross-compilation environment.

1. You can find the source code of our project in the `src` directory of this repository. We recommend you to compile not in this folder but in a copy of it as you may want to keep a clean copy of the repository. In the explanations that follow, we will name this directory `yourSrc`.
```
cp -r <path_to_the_repository>/os-robot-challenge/src <path_to_yourSrc>/yourSrc
```

2. First clone the repository `ev3dev-c` in `yourSrc`.
```
cd <path_to_yourSrc>/yourSrc
GIT_SSL_NO_VERIFY=true git clone https://github.com/in4lio/ev3dev-c.git
```

3. Then launch the cross-compilation environment .
```
docker run -e LOCAL_USER_ID=`id -u $USER` --rm -it -v <path_to_yourSrc>/yourSrc:/src -w /src ev3dev/debian-jessie-cross
```

4. If you just cloned the `ev3dev-c` repository, run the following set of commands to build `ev3dev-c` API.
```
cd ev3dev-c/source/ev3
make
cd ../../..
```

5. Install the `ev3dev-c` API by running the `install.sh` script.
```
bash install.sh
```

6. Finally, you can have a look at the make file and build an executable. For building the full application, just type:
```
make
```
